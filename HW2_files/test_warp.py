import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.io as sio
from lucas import *

# Load images I1,I2
IMG = sio.loadmat('HW2_PART1_IMAGES.mat')
I1 = IMG['I1']
I2 = IMG['I2']
WindowSize = (3, 3)


# little test
u1 = np.transpose(np.ones((I1.shape[0], int(I1.shape[1] / 2))) * -10)
u2 = np.transpose(np.zeros((I1.shape[0], int(I1.shape[1] / 2))))
u = np.transpose(np.vstack((u1, u2)))
v1 = np.transpose(np.ones((I1.shape[0], int(I1.shape[1] / 2))) * -50)
v2 = np.transpose(np.zeros((I1.shape[0], int(I1.shape[1] / 2))))
v = np.transpose(np.vstack((v1, v2)))
warped_image = WarpImage(I1, u, v)

# dp = LucasKanadeStep(I1, I2, WindowSize)
# warped_image = WarpImage(I1, dp[0], dp[1])
fig, ax = plt.subplots(1, 3)
ax[0].imshow(I1)
ax[1].imshow(warped_image)
ax[2].imshow(I2)
plt.show()
