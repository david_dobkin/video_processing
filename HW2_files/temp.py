import numpy as np
from numpy.lib.stride_tricks import as_strided

xsize = 5
ysize = 5
xstep = 1
ystep = 1
A = np.arange(400).reshape((20, 20))
print(A)
shape = (int((A.shape[0] - xsize + 1) / xstep), int((A.shape[1] - ysize + 1) / ystep), xsize, ysize)
strides = (A.strides[0] * xstep, A.strides[1] * ystep, A.strides[0], A.strides[1])
all_windows = as_strided(A, shape, strides)

print(all_windows)
