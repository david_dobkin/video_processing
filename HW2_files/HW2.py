import scipy.io as sio
from ex2_functions import *
#
#
# # FILL IN YOUR ID
# # ID1 = 205825276
# # ID2 = 314487455
#
#
# #####################################PART 1: Lucas-Kanade Optical Flow################################################
#
# # Load images I1,I2
IMG = sio.loadmat('HW2_PART1_IMAGES.mat')
I1 = IMG['I1']
I2 = IMG['I2']
# u_demo = np.ones_like(I1)*0.5
# v_demo = np.zeros_like(I1)
# I_demo = WarpImage(I1, u_demo, v_demo)
#
# Choose parameters
WindowSize = (3, 3)  # Add your value here!
MaxIter = 3  # Add your value here!
NumLevels = 3  # Add your value here!
#
# Compute optical flow using LK algorithm
(u, v) = LucasKanadeOpticalFlow(I1, I2, WindowSize, MaxIter, NumLevels)
#
#
# # Warp I2
I2_warp = WarpImage(I2, u, v)
#
# # The RMS should decrease as the warped image (I2_warp) should be more similar to I1
print('RMS of original frames: '+ str(np.sum(np.sum(np.abs((I1-I2)**2)))))
min_rms = np.sum(np.sum(np.abs((I1-I2_warp)**2)))
print('RMS of warped frame: ' + str(min_rms))

############################################3PART 2: Video Stabilization################################################
#
# Choose parameters
WindowSize = (3, 3)  # Add your value here!
MaxIter = 3  # Add your value here!
NumLevels = 4  # Add your value here!
#
# Load video file
InputVidName = 'input.avi'
input_vid_path = InputVidName
InputVid = cv2.VideoCapture(input_vid_path)
# Stabilize video - save the stabilized video inside the function
StabilizedVid = LucasKanadeVideoStabilization(InputVid, WindowSize, MaxIter, NumLevels)
# Release everything if job is finished
InputVid.release()
StabilizedVid.release()
