import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.io as sio
from lucas import *
import numpy as np
import cv2
# Load images I1,
ImageDirPath = 'eval-data-gray\\Basketball\\'
I1_PATH = ImageDirPath + 'frame10.png'
I2_PATH = ImageDirPath + 'frame11.png'
test_I1 = cv2.imread(I1_PATH, 0)
test_I2 = cv2.imread(I2_PATH, 0)
IMG = sio.loadmat('HW2_PART1_IMAGES.mat')
I1 = IMG['I1']
I2 = IMG['I2']
WindowSize = (3, 3)
dp = LucasKanadeStep(test_I1, test_I2, WindowSize)
u = dp[..., 0]
v = dp[..., 1]


mag_vel_img = np.sqrt(np.square(u) + np.square(v))
mag_vel_img = (mag_vel_img / np.max(mag_vel_img))*255
u_direction_vel_img = np.where(mag_vel_img != 0, u/mag_vel_img, 0)
v_direction_vel_img = np.where(mag_vel_img != 0, u/mag_vel_img, 0)
fig, ax = plt.subplots(1, 2)
ax[0].set_aspect('equal')
ax[0].imshow(np.abs(test_I2 - test_I1))
ax[1].set_aspect('equal')
ax[1].imshow(mag_vel_img)

plt.show(block=True)
plt.figure(2)
plt.quiver(np.abs(test_I2 - test_I1), u,v)
plt.show(block=True)