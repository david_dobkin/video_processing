from lucas import *
import scipy.io as sio
import matplotlib.pyplot as plt


IMG = sio.loadmat('HW2_PART1_IMAGES.mat')
I1 = IMG['I1']

num_of_levels = 4
pyramid = build_pyramid(I1, num_of_levels)
# pyramid = [I1, I1, I1, I1]
fig, ax = plt.subplots(1, num_of_levels)
for level in range(num_of_levels):
    ax[level].imshow(pyramid[level])
plt.show()
