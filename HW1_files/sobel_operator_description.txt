Sobel operator is used for edge detection, and it is trying to find sharp changes in the intensity of an image. 
The operator computed on each pixel in both directions, x and y, and then it root sum of square of the gradients it computed in both directions. 
The operator is symmetric from its center, and it emphasized on the center. Sobel operator is an approximation to derivative of the image.
A high value indicates an edge, and a low value indicated that it is used on a pixel with a neighborhood with almost the same intensity.

