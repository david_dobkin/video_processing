#########################################################
################ File for ex1_Q3_functions ##############


## accepts an image, and returns a binary matrix (same size as
# the original image dimensions) with '1' denoting corner pixels and '0'
# denoting all other pixels.
# IMPORTANT - DO NOT USE ANY FIGURE COMMANDS, ONLY SUBPLOT.
#             SETTING ANOTHER figure COMMAND WILL MESS UP THE SAVING
#             PROCESS AND YOU WILL LOSE POINTS IF THAT HAPPENS!
# ID1 = 314487455
# ID2 = 205825276

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import cv2
from matplotlib.patches import Circle


def non_max_supression(response_image, grid_shape):
    image_dim = response_image.shape
    grid_level_0 = int(image_dim[0] / grid_shape[0])
    grid_level_1 = int(image_dim[1] / grid_shape[1])
    offset_list = []
    nms_bin_matrix = np.zeros(image_dim)
    for i in range(grid_level_0):
        for j in range(grid_level_1):
            wind_response_im = response_image[i * grid_shape[0]:(i + 1) * grid_shape[0],
                               j * grid_shape[1]:((j + 1) * grid_shape[1])]
            offset_0, offset_1 = np.unravel_index(np.argmax(abs(wind_response_im)), wind_response_im.shape)
            offset_list.append((i * grid_shape[0] + offset_0, j * grid_shape[1] + offset_1))
            nms_bin_matrix[i * grid_shape[0] + offset_0][j * grid_shape[1] + offset_1] = 1
    nms_matrix = np.multiply(nms_bin_matrix, response_image)
    return nms_matrix, nms_bin_matrix


def myHarrisCornerDetector(I, K, threshold, use_grid):
    if len(I.shape) == 3:
        I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
    # Step 1:
    # Compute Ix and Iy for all image
    Iy, Ix = np.gradient(I)
    # Step 2:
    g = np.ones((5, 5))
    # Compute M matrix
    s_xx = signal.convolve2d(np.multiply(Ix, Ix), g, mode='same')
    s_yy = signal.convolve2d(np.multiply(Iy, Iy), g, mode='same')
    s_xy = signal.convolve2d(np.multiply(Ix, Iy), g, mode='same')
    # Step 3:
    # Compute response image: det(M) - k*(trace(M))^2
    response_image = np.multiply(s_xx, s_yy) - np.multiply(s_xy, s_xy) - \
                     K * np.multiply((s_xx + s_yy), (s_xx + s_yy))
    # Step 4:
    # 4a: if grid is used:
    if use_grid:
        grid_shape = (25, 25)
        # Find max of each repsponse_image
        nms_image, nms_bin_image = non_max_supression(response_image, grid_shape)
        after_thd_image = np.where(nms_image > threshold, 1, 0)
    else:
        after_thd_image = np.where(response_image > threshold, 1, 0)
    return after_thd_image


# createCornerPlots creates a plot with 2 subplots (on the left show I_1
# with the corner points and on the right show I2 with the corner points)

def createCornerPlots(I1, I1_CORNERS, I2, I2_CORNERS):
    if len(I1.shape) == 3:
        I1 = cv2.cvtColor(I1, cv2.COLOR_BGR2RGB)
    else:
        I1 = cv2.cvtColor(I1, cv2.COLOR_GRAY2RGB)
    fig, ax = plt.subplots(1, 2)
    ax[0].set_aspect('equal')
    ax[1].set_aspect('equal')
    corners = np.nonzero(I1_CORNERS)
    corners = np.column_stack(corners)
    ax[0].imshow(I1)
    for corner in corners:
        circ = Circle((corner[1], corner[0]), 5, color='r')
        ax[0].add_patch(circ)
    if len(I1.shape) == 3:
        I2 = cv2.cvtColor(I2, cv2.COLOR_BGR2RGB)
    else:
        I2 = cv2.cvtColor(I2, cv2.COLOR_GRAY2RGB)
    corners = np.nonzero(I2_CORNERS)
    corners = np.column_stack(corners)
    ax[1].imshow(I2)
    for corner in corners:
        circ = Circle((corner[1], corner[0]), 5, color='r')
        ax[1].add_patch(circ)
    plt.show(block=True)
