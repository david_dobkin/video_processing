# FILL IN YOUR ID
# ID1 = 314487455
# ID2 = 205825276


import cv2
from matplotlib import pyplot as plt
import numpy as np
from scipy import signal

def Q2A(path):
    cap = cv2.VideoCapture(path)
    codec = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out_size = (width, height)
    out = cv2.VideoWriter('Vid1_Binary.avi', codec, fps, out_size, isColor=False)
    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            ret2, frame = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            out.write(frame)
        else:
            print('finished working on all frames')
            break
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()


def Q2B(path):
    cap = cv2.VideoCapture(path)
    codec = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out_size = (width, height)
    out = cv2.VideoWriter('Vid2_Grayscale.avi', codec, fps, out_size, isColor=False)
    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            out.write(frame)
        else:
            print('finished working on all frames')
            break
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()


def Q2C(path):
    cap = cv2.VideoCapture(path)
    codec = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out_size = (width, height)
    out = cv2.VideoWriter('Vid3_Sobel.avi', codec, fps, out_size, isColor=False)
    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            deriv_X_filter = np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
            gray_deriv_X = signal.convolve2d(frame, deriv_X_filter, mode='same')  # Keep output image with same size.
            deriv_Y_filter = deriv_X_filter.copy().transpose()
            gray_deriv_Y = signal.convolve2d(frame, deriv_Y_filter, mode='same')  # Keep output image with same size.
            frame = np.sqrt(np.square(gray_deriv_X) + np.square(gray_deriv_Y))
            frame = np.absolute(frame)
            frame = np.uint8(frame)
            out.write(frame)
        else:
            print('finished working on all frames')
            break
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()


input_path = 'atrium.avi'
Q2A(input_path)
Q2B(input_path)
Q2C(input_path)
