### This module is responsible for the video stabliztion of final video processing project
### import libraries

from src.lucas_kanade import *
import os

### Import stabilztion parameters
import config as cfg


def LucasKanadeVideoStabilization(InputVid,WindowSize,MaxIter,NumLevels, alg="LK"):
    codec = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    fps = InputVid.get(cv2.CAP_PROP_FPS)
    width = int(InputVid.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(InputVid.get(cv2.CAP_PROP_FRAME_HEIGHT))
    crop_shape = cfg.stablize_params["CROP_SHAPE"]
    out_size = (width - 2*crop_shape[0], height-2*crop_shape[1])
    output_path = os.path.join(cfg.general_params["OUTPUT_DIR"],
                                cfg.general_params["STABLIZED_VID_NAME"])

    out = cv2.VideoWriter(output_path, codec, fps, out_size, isColor=False)
    ret, first_frame = InputVid.read()
    first_frame = cv2.cvtColor(first_frame, cv2.COLOR_BGR2GRAY)
    prev_frame = first_frame
    frame_index = 0
    while InputVid.isOpened():
        ret, frame = InputVid.read()
        if ret is True:
            # Get new frame from lucas kanade
            cur_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            if(alg == 'LK'):
                u, v = LucasKanadeOpticalFlow(image1=cur_frame, image2=prev_frame,
                                          window_size=WindowSize,
                                          max_iterations=MaxIter, num_levels=NumLevels,
                                          tau=0, K=1e-3)
            elif(alg == "Farneback"):
                flow = cv2.calcOpticalFlowFarneback(prev_frame, cur_frame, flow=None, pyr_scale=0.5, levels=NumLevels,
                    winsize=WindowSize[0], iterations=MaxIter,  poly_n=5, poly_sigma=1.2, flags=0)
                u, v = flow(...,0), flow(...,1)
            u = np.where(np.isnan(u), 0, u)
            v = np.where(np.isnan(v), 0, v)
            u = np.where(np.abs(u) > width, 0, u)
            v = np.where(np.abs(v) > height, 0, v)
            const_u, const_v = np.mean(u), np.mean(v)
            const_u_mat = np.ones_like(u) * const_u
            const_v_mat = np.ones_like(v) * const_v
            stable_frame = WarpImage(prev_frame, u, v)
            cropped_stable_frame = stable_frame[crop_shape[0]:-crop_shape[0],
                                                crop_shape[1]:-crop_shape[1]]
            out.write(cropped_stable_frame)
            string_to_print = "Work on frame: " + str(frame_index)
            frame_index = frame_index + 1
            print(string_to_print)
            prev_frame = cur_frame
        else:
            print('finished working on all frames')
            break
    cv2.destroyAllWindows()
    # Return result video
    return out

# Moving average calculatin of trajectory smoothing
def movingAverage(curve, radius): 
  window_size = 2 * radius + 1
  # Define the filter 
  f = np.ones(window_size)/window_size 
  # Add padding to the boundaries 
  curve_pad = np.lib.pad(curve, (radius, radius), 'edge') 
  # Apply convolution 
  curve_smoothed = np.convolve(curve_pad, f, mode='same') 
  # Remove padding 
  curve_smoothed = curve_smoothed[radius:-radius]
  # return smoothed curve
  return curve_smoothed 


def smooth(trajectory): 
  smoothed_trajectory = np.copy(trajectory) 
  # Filter the x, y and angle curves
  for i in range(3):
    smoothed_trajectory[:,i] = movingAverage(trajectory[:,i], radius=cfg.stablize_params["SMOOTHING_RADIUS"])

  return smoothed_trajectory


def fixBorder(frame):
  s = frame.shape
  # Scale the image 4% without moving the center
  T = cv2.getRotationMatrix2D((s[1]/2, s[0]/2), 0, 1.04)
  frame = cv2.warpAffine(frame, T, (s[1], s[0]))
  return frame


def PFMVideoStabilzation(InputVid):
    codec = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    fps = InputVid.get(cv2.CAP_PROP_FPS)

    # Gent number of frames
    n_frames = int(InputVid.get(cv2.CAP_PROP_FRAME_COUNT))

    w = int(InputVid.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(InputVid.get(cv2.CAP_PROP_FRAME_HEIGHT))
    crop_shape = cfg.stablize_params["CROP_SHAPE"]
    out_size = (w, h)
    output_path = os.path.join(cfg.general_params["OUTPUT_DIR"],
                                cfg.general_params["STABLIZED_VID_NAME"])

    out = cv2.VideoWriter(output_path, codec, fps, out_size)
    ret, prev_frame = InputVid.read()
    prev_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
    frame_index = 0
    # Pre-define transformation-store array
    transforms = np.zeros((n_frames-1, 3), np.float32)
    for i in range(n_frames-2):
        # Detect feature points in previous frame
        prev_pts = cv2.goodFeaturesToTrack(prev_gray,
                                     maxCorners=200,
                                     qualityLevel=0.01,
                                     minDistance=30,
                                     blockSize=3)
        # Read next frame
        success, curr = InputVid.read() 
        if not success:
            # Break if finished all frames
            print("Stopped reading frames") 
            break 
        # Convert to grayscale
        curr_gray = cv2.cvtColor(curr, cv2.COLOR_BGR2GRAY)
        # Calculate optical flow (i.e. track feature points)
        curr_pts, status, err = cv2.calcOpticalFlowPyrLK(prev_gray, curr_gray, prev_pts, None) 
        # Sanity check
        assert prev_pts.shape == curr_pts.shape 
        # Filter only valid points
        idx = np.where(status==1)[0]
        prev_pts = prev_pts[idx]
        curr_pts = curr_pts[idx]
        # Find transformation matrix
        m = cv2.estimateRigidTransform(prev_pts, curr_pts, fullAffine=False) #will only work with OpenCV-3 or less
        # Extract traslation
        dx = m[0,2]
        dy = m[1,2]
        # Extract rotation angle
        da = np.arctan2(m[1,0], m[0,0])
        # Store transformation
        transforms[i] = [dx,dy,da]
        # Move to next frame
        prev_gray = curr_gray
        print("Frame: " + str(i) +  "/" + str(n_frames) + " -  Tracked points : " + str(len(prev_pts)))
    else:
        print('finished working on all frames')

    
    # Compute trajectory using cumulative sum of transformations
    trajectory = np.cumsum(transforms, axis=0)
    # Smooth trajectory for stablization
    smoothed_trajectory = smooth(trajectory)
    # Calculate difference in smoothed_trajectory and trajectory
    difference = smoothed_trajectory - trajectory
    # Calculate newer transformation array
    transforms_smooth = transforms + difference

    # Create new frame by camera adjusments

    # Reset stream to first frame 
    InputVid.set(cv2.CAP_PROP_POS_FRAMES, 0) 
 
    # Write n_frames-1 transformed frames
    for i in range(n_frames-2):
        # Read next frame
        success, frame = InputVid.read() 
        if not success:
            print("Break during stablized video creation")
            break
        # Extract transformations from the new transformation array
        dx = transforms_smooth[i,0]
        dy = transforms_smooth[i,1]
        da = transforms_smooth[i,2]
        # Reconstruct transformation matrix accordingly to new values
        m = np.zeros((2,3), np.float32)
        m[0,0] = np.cos(da)
        m[0,1] = -np.sin(da)
        m[1,0] = np.sin(da)
        m[1,1] = np.cos(da)
        m[0,2] = dx
        m[1,2] = dy
        # Apply affine wrapping to the given frame
        frame_stabilized = cv2.warpAffine(frame, m, (w,h))
        # Fix border artifacts
        frame_stabilized = fixBorder(frame_stabilized) 
        # Write the frame to the file
        frame_out = cv2.hconcat([frame, frame_stabilized])
        # If the image is too big, resize it.
        if(frame_out.shape[1] > 1920): 
            frame_out = cv2.resize(frame_out, (int(frame_out.shape[1]/2), int(frame_out.shape[0]/2)));
        cv2.imshow("Before and After", frame_out)
        cv2.waitKey(10)
        out.write(frame_out)

    
    cv2.destroyAllWindows()
    # Return result video
    return out




def StablizeVid(input_vid):
    
    if cfg.stablize_params["ALGORITHM"] == "LUCAS_KANADE":
        stablize_vid = LucasKanadeVideoStabilization(InputVid=input_vid, WindowSize=cfg.stablize_params["WINDOW_SIZE"],
        MaxIter=cfg.stablize_params["MAX_ITER"], NumLevels=cfg.stablize_params["NUM_LEVELS"], alg="LK")

    elif cfg.stablize_params["ALGORITHM"] == "LUCAS_KANADE_FEATURES":
        # To do use feature detection lucas kande

        stablize_vid = 0
        pass
    elif cfg.stablize_params["ALGORITHM"] == "PFM":
         # Point Feature Matching
         # Get frame count
         stabilzed_vid = PFMVideoStabilzation(InputVid=input_vid)

    elif cfg.stablize_params["ALGORITHM"] == "Farneback":
        LucasKanadeVideoStabilization(InputVid=input_vid, WindowSize=cfg.stablize_params["WINDOW_SIZE"],
        MaxIter=cfg.stablize_params["MAX_ITER"], NumLevels=cfg.stablize_params["NUM_LEVELS"], alg="Farneback")
        
    else:
        stablized_vid = 0
    return stablize_vid

