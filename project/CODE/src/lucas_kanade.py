import cv2
import numpy as np


def build_pyramid(image, num_levels):
    pyramid_stack = []
    pyramid_stack.insert(0, image)
    for level in range(num_levels - 1):
        temp = cv2.pyrDown(pyramid_stack[level])
        pyramid_stack.insert(level + 1, temp)    # Concat at end
    return pyramid_stack


def WarpImage(image, u, v):
    warped_image = None
    u = np.float32(u)
    v = np.float32(v)
    org_x = np.arange(image.shape[0], dtype=np.float32)
    org_y = np.arange(image.shape[1], dtype=np.float32)
    grid = np.meshgrid(org_y, org_x)
    grid[0] -= u
    grid[1] -= v
    warped_image = cv2.remap(image, grid[0], grid[1], cv2.INTER_LINEAR)
    return warped_image


def LucasKanadeStep(im1, im2, win=7, tau=1e-3, K=0.05):
    ix = np.zeros(im1.shape)
    iy = np.zeros(im1.shape)
    it = np.zeros(im1.shape)

    ix[1:-1, 1:-1] = (im1[1:-1, 2:] - im1[1:-1, :-2]) / 2
    iy[1:-1, 1:-1] = (im1[2:, 1:-1] - im1[:-2, 1:-1]) / 2
    it[1:-1, 1:-1] = im1[1:-1, 1:-1] - im2[1:-1, 1:-1]

    grad_mults = np.zeros(im1.shape + (5,))
    grad_mults[..., 0] = cv2.GaussianBlur(ix * ix, (5, 5), 3)
    grad_mults[..., 1] = cv2.GaussianBlur(iy * iy, (5, 5), 3)
    grad_mults[..., 2] = cv2.GaussianBlur(ix * iy, (5, 5), 3)
    grad_mults[..., 3] = cv2.GaussianBlur(ix * it, (5, 5), 3)
    grad_mults[..., 4] = cv2.GaussianBlur(iy * it, (5, 5), 3)

    cum_grad_mults = np.cumsum(np.cumsum(grad_mults, axis=0), axis=1)
    win_grad_mults = (cum_grad_mults[2 * win + 1:, 2 * win + 1:] -
                      cum_grad_mults[2 * win + 1:, :-1 - 2 * win] -
                      cum_grad_mults[:-1 - 2 * win, 2 * win + 1:] +
                      cum_grad_mults[:-1 - 2 * win, :-1 - 2 * win])

    u = np.zeros(im1.shape)
    v = np.zeros(im1.shape)

    ixx = win_grad_mults[..., 0]
    iyy = win_grad_mults[..., 1]
    ixy = win_grad_mults[..., 2]
    ixt = -win_grad_mults[..., 3]
    iyt = -win_grad_mults[..., 4]

    M_det = ixx * iyy - ixy ** 2
    temp_u = iyy * (-ixt) + (-ixy) * (-iyt)
    temp_v = (-ixy) * (-ixt) + ixx * (-iyt)
    K = 0.05
    min_eig_val_est = M_det - K*(ixx + iyy)*(ixx + iyy)

    op_flow_x = np.where(np.abs(min_eig_val_est) > tau, temp_u / M_det, 0)
    op_flow_y = np.where(np.abs(min_eig_val_est) > tau, temp_v / M_det, 0)

    u[win + 1: -1 - win, win + 1: -1 - win] = op_flow_x[:-1, :-1]
    v[win + 1: -1 - win, win + 1: -1 - win] = op_flow_y[:-1, :-1]

    return u, v


def LucasKanadeOpticalFlow(image1, image2, window_size, max_iterations, num_levels, tau=0, K=0):
    u, v = None, None
    pyramid1 = build_pyramid(image1, num_levels)
    pyramid2 = build_pyramid(image2, num_levels)
    for level in reversed(range(num_levels)):     # Start from the level with least resolutions
        if u is None:
            image2_warp = pyramid2[num_levels - 1]
            flow = np.zeros_like([image2_warp, image2_warp])
        else:
            u = cv2.resize(u, (pyramid1[level].shape[1], pyramid1[level].shape[0]),
                           interpolation=cv2.INTER_AREA)
            v = cv2.resize(v, (pyramid1[level].shape[1], pyramid1[level].shape[0]),
                           interpolation=cv2.INTER_AREA)
            image2_warp = WarpImage(pyramid2[level], u, v)
        for _ in range(max_iterations):
            g1 = pyramid1[level]
            g2 = image2_warp
            du, dv = LucasKanadeStep(g2, g1, window_size[0], tau=tau, K=K)
            u = du if u is None else u + du
            v = dv if v is None else v + dv
            image2_warp = WarpImage(pyramid2[level], u, v)
    return u, v