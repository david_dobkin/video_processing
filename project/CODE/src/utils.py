import logging
import cv2


logger = logging.getLogger(__name__)


def load_video(video_path):
    cap = cv2.VideoCapture(video_path)
    codec = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    fps = cap.get(cv2.CAP_PROP_FPS)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out_size = (width, height)
    out = cv2.VideoWriter('Vid1_Binary.avi', codec, fps, out_size, isColor=False)
    while cap.isOpened():
        ret, frame = cap.read()
        if ret is True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            ret2, frame = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            out.write(frame)
        else:
            print('finished working on all frames')
            break
    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()
