import logging
import cv2
import numpy as np
import random
import time
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as plt
import GeodisTK
from skimage.measure import label, regionprops
import skimage.io as io

# todo: add hyper-parameters file
logger = logging.getLogger(__name__)
img = None
drawing_bg = True
drawing = False
bg_pixels = []
fg_pixels = []
done = False


# todo: remove option to draw outside the image or discard
def draw_circle(event, x, y, flags, params):
    global drawing_bg, drawing, done
    done = False
    if event == cv2.EVENT_LBUTTONDOWN:
        if not done:
            drawing = True
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing:
            if drawing_bg:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
                bg_pixels.append((x, y))
            else:
                cv2.circle(img, (x, y), 5, (0, 255, 0), -1)
                fg_pixels.append((x, y))

    elif event == cv2.EVENT_LBUTTONUP:
        if drawing_bg:
            drawing_bg = False
            drawing = False
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
            bg_pixels.append((x, y))
        else:
            drawing = False
            done = True
            cv2.circle(img, (x, y), 5, (0, 255, 0), -1)
            fg_pixels.append((x, y))


def get_input_scribbles():
    global img, done
    cv2.namedWindow('image')
    cv2.setMouseCallback('image', draw_circle)
    while True:
        cv2.imshow('image', img)
        if cv2.waitKey(20) & 0xFF == 27 or done:
            break
    cv2.destroyAllWindows()


def kde(image, points, bandwidth=0.5, **kwargs):
    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    values = np.array([])
    for i in range(len(points) - 1):
        x, y = points[i]
        values = np.append(values, image[y, x])
    grid = np.linspace(0, 255, 5000)
    kde_skl.fit(values[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    return kde_skl


def score_kde(values, kde_density):
    size = values.shape
    values = values.flatten()
    log_pdf = kde_density.score_samples(values[:, np.newaxis])
    log_pdf = log_pdf.reshape(size)
    return np.exp(log_pdf)


def generate_mask_from_points(image, size, points):
    mask = np.zeros(size)
    for point in points:
        mask[point[1], point[0]] = image[point[1], point[0]]
    plt.imshow('mask', mask)


# todo: find the best threshold
def geodesic_distance2d(input_img, seed_pos):
    img_float = np.asanyarray(input_img, np.float32)
    seed_map = np.zeros((img_float.shape[0], img_float.shape[1]), np.uint8)
    for seed in seed_pos:
        seed_map[seed[1], seed[0]] = 1
    distance_map = GeodisTK.geodesic2d_raster_scan(img_float, seed_map, 0.2, 2)
    return distance_map


def do_matting(image_x, background_image, alpha_m):
    return image_x * alpha_m + background_image * (1 - alpha_m)
# def gradient(mat):
#     ix[1:-1, 1:-1] = (im1[1:-1, 2:] - im1[1:-1, :-2]) / 2
#     iy[1:-1, 1:-1] = (im1[2:, 1:-1] - im1[:-2, 1:-1]) / 2
#     return ix, iy


def refinement(diluted_perimeter, prob_bg, prob_fg, dist_bg, dist_fg, r=2):
    pass


cap = cv2.VideoCapture('../../Input/Stabilized_Example_INPUT.avi')

width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

if cap.isOpened():
    ret, img = cap.read()
if not ret:
    print('failed to load video')
    exit()


# img = cv2.imread('../../Input/image_for_matting.png')
background = cv2.imread('../../Input/background.jpg')
img_copy = img.copy()
img_rgb = cv2.cvtColor(img_copy, cv2.COLOR_BGR2RGB)
background_rgb = cv2.cvtColor(background, cv2.COLOR_BGR2RGB)
get_input_scribbles()
img = cv2.cvtColor(img_copy, cv2.COLOR_BGR2HSV)
# background = cv2.cvtColor(background, cv2.COLOR_BGR2HSV)
# background = background[:418, :250, :]
# background = background[:, :, 2]
img_v = img_copy[:, :, 2]
# generate_mask_from_points(img_v, (height, width), fg_pixels)
# todo: check kde optimal bandwidth
kde_density_bg = kde(img_v.copy(), bg_pixels, bandwidth=0.5)
kde_density_fg = kde(img_v.copy(), fg_pixels, bandwidth=0.5)

pixels_score_bg = score_kde(img_v.copy(), kde_density_bg)
pixels_score_fg = score_kde(img_v.copy(), kde_density_fg)

pixels_score_sum = pixels_score_bg + pixels_score_fg
pixels_prob_bg = pixels_score_bg / (pixels_score_sum + 10 ** -3)
pixels_prob_fg = pixels_score_fg / (pixels_score_sum + 10 ** -3)

(thresh, pixels_prob_bg_binary) = cv2.threshold(pixels_prob_bg * 255, 0, 255, cv2.THRESH_BINARY)
(thresh, pixels_prob_fg_binary) = cv2.threshold(pixels_prob_fg * 255, 0, 255, cv2.THRESH_BINARY)
# kde_bg = kde(bg_pixels, bandwidth=0.5)
fig, ax = plt.subplots(1, 3)
ax[0].imshow(img_v)
ax[1].imshow(pixels_prob_bg_binary)
ax[2].imshow(pixels_prob_fg_binary)
plt.show()
distance_bg = geodesic_distance2d(pixels_prob_bg.copy() * 255, bg_pixels)
distance_fg = geodesic_distance2d(pixels_prob_fg.copy() * 255, fg_pixels)
fig, ax = plt.subplots(1, 7)
ax[0].imshow(img_rgb)
ax[1].imshow(distance_bg)
# ax[2].imshow(distance_fg)
fg = distance_fg - distance_bg
fg = np.where(fg <= 0, img_v, 0)
ax[3].imshow(fg)


_, fg_binary = cv2.threshold(fg, 0, 255, cv2.THRESH_BINARY)
ax[4].imshow(fg_binary)
kernel = np.ones((5, 5), np.uint8)  # using bigger kernel to do the dilation after the perimeter extraction
fg_binary_dilated = cv2.dilate(fg_binary, kernel, iterations=1)
fg_perimeter = fg_binary_dilated - fg_binary
fg_inside = fg_binary_dilated - fg_perimeter
ax[5].imshow(fg_perimeter)
alpha_map = np.where(fg_inside != 0, 1, 0)  # extracting all fg area to alpha map
ax[2].imshow(alpha_map)
r = 2
distance_perimeter_bg = np.where(fg_perimeter != 0, distance_bg, 0)
distance_perimeter_fg = np.where(fg_perimeter != 0, distance_fg, 0)
prob_perimeter_bg = np.where(fg_perimeter != 0, pixels_prob_bg, 0)
prob_perimeter_fg = np.where(fg_perimeter != 0, pixels_prob_fg, 0)
w_bg = prob_perimeter_bg * (distance_perimeter_bg ** (1 / r))
w_fg = prob_perimeter_fg * (distance_perimeter_fg ** (1 / r))
alpha = w_fg / (w_fg + w_bg + 10 ** -3)
alpha_map = np.logical_or(alpha_map, alpha)
# fg = fg / 255
alpha_map = np.repeat(alpha_map[:, :, np.newaxis], 3, axis=2)
matted_img = do_matting(img_rgb, background_rgb, alpha_map)
ax[6].imshow(matted_img)
plt.show()

fig, ax = plt.subplots(1, 3)
ax[0].imshow(img_rgb)
ax[1].imshow(background_rgb)
ax[2].imshow(matted_img)
plt.show()


while True:
    time.sleep(2)