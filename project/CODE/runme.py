from src.utils import *
import logging
from src.matting import extract_trimap


def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s - %(name)s - %(levelname)s| %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger(__name__)
    logger.debug('start')
    input_video_path = '../Input/Stabilized_Example_INPUT.avi'
    input_background_path = '../Input/background.jpg'
    trimap_video = extract_trimap(input_video_path)


if __name__ == "__main__":
    main()