

general_params ={
    "INPUT_DIR": "Input",
    "INPUT_FILE_NAME": "INPUT.avi",
    "OUTPUT_DIR": "Outputs",
    "STABLIZED_VID_NAME": "StabilizedVid" + ".avi"
}

stablize_params={
    "ALGORITHM": "PFM",
    "CROP_SHAPE": (10, 10),
    # Lucas-kanade parameters
    "WINDOW_SIZE": (3, 3),
    "MAX_ITER": 3,
    "NUM_LEVELS": 3,
    
    ## PFM Algorithm
    "SMOOTHING_RADIUS": 5
}