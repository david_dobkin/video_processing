import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

metric = {
    "SSD": 1,
    "SAD": 2,
    "EXP": 3,
}
IMAGE = 'input.jpg'
TEMPLATE = 'Template3.jpg'
METRIC = "SSD"
BLURRING_FILTER_SIZE = 10


def print_progress(height, width, i, j):
    if 0 == (i*width + j) % 10000:
        print("processed {0} pixels, out of {1} pixels.".format(i*height+j, height*width,))


def calculate_score_matrix(image, template, selected_metric=None):
    (height, width) = image.shape
    (template_height, template_width) = template.shape
    image_score = np.zeros((height, width))
    h_start_index = 0
    h_stop_index = height - template_height
    w_start_index = 0
    w_stop_index = width - template_width

    for i in range(h_start_index, h_stop_index):
        for j in range(w_start_index, w_stop_index):
            image_score[i, j] = calculate_score_per_pixel(image, template, i, j, selected_metric)
            print_progress(h_stop_index - h_start_index, w_stop_index - w_start_index, i, j)

    image_score = image_score[h_start_index:h_stop_index, w_start_index:w_stop_index]
    return image_score


def calculate_score_per_pixel(image, template, x, y, selected_metric=None):
    (template_height, template_width) = template.shape
    temp_image = image[x:x+template_height, y:y+template_width]
    if metric["SAD"] == metric[selected_metric]:
        pixel_score = int(np.sum(np.abs(np.abs(np.subtract(temp_image, template)))))
    elif metric["SSD"] == metric[selected_metric]:
        pixel_score = int(np.sum(np.abs(np.power(np.subtract(temp_image, template), 2))))
    elif metric["EXP"] == metric[selected_metric]:
        pixel_score = np.sum(np.exp(-1 * np.power(np.subtract(temp_image, template),2)))
    else:
        raise ValueError("Please select a valid metric")
    return pixel_score


def get_score(image_path, template_path, filter_size=1, metric=None):
    image = cv2.imread(image_path)
    template = cv2.imread(template_path)
    input_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    input_gray_b = average_filter_blur(input_gray, filter_size)
    template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('gray_template.jpg', template_gray)
    cv2.imwrite('gray_input_' + str(filter_size) + '_blurring_window.jpg', input_gray_b)
    score = calculate_score_matrix(input_gray_b, template_gray, metric)
    score = score - score.min()
    return score


def plot_best_score_rect(image_path, template_path, x, y, blurring_filter_size_=BLURRING_FILTER_SIZE):
    im = np.array(Image.open(image_path), dtype=np.uint8)
    template = np.array(Image.open(template_path), dtype=np.uint8)
    rect_size = template.shape[:-1]
    # Create figure and axes
    fig, ax = plt.subplots(1)
    # Display the image
    ax.imshow(im)
    # Create a Rectangle patch
    rect = patches.Rectangle((x, y), rect_size[1], rect_size[0], linewidth=1, edgecolor='r', facecolor='none')
    # Add the patch to the Axes
    ax.add_patch(rect)
    plot_name = "{0}_{1}_{2}_blurred_by_{3}_best_score_rect.png".format(IMAGE.replace(".jpg", ""),
                                                                        TEMPLATE.replace(".jpg", ""),
                                                                        METRIC, blurring_filter_size_)
    plt.savefig(plot_name)
    # plt.show()
    plt.close('all')


def plot_score_surface(score, best_fit_index, blurring_filter_size_=BLURRING_FILTER_SIZE):
    best_score = score.min()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x = np.arange(0, score.shape[1], 1)
    y = np.arange(0, score.shape[0], 1)
    X, Y = np.meshgrid(x, y)

    # Plot the surface.
    surf = ax.plot_surface(X, Y, score, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.set_zlim(score.min(), score.max())
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    ax.scatter(best_fit_index[1], best_fit_index[0], best_score, s=10, c='g', marker="o", label='minimal_value')
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plot_name = "{0}_{1}_{2}_blurred_by_{3}_score_surface.png".format(IMAGE.replace(".jpg", ""),
                                                                      TEMPLATE.replace(".jpg", ""),
                                                                      METRIC, blurring_filter_size_)
    plt.savefig(plot_name)
    # plt.show()
    plt.close('all')


def average_filter_blur(image, blurring_filter_size):
    blurring_filter = (1 / blurring_filter_size ** 2) * np.ones([blurring_filter_size, blurring_filter_size])
    blurred_image = cv2.filter2D(image, -1, blurring_filter)
    return blurred_image


if __name__ == "__main__":

    for filter_size in [1, 5, 10, 50]:
        # Calculate the score
        score = get_score(IMAGE, TEMPLATE, filter_size, METRIC)

        # Find the pixel that had the best score
        best_fit_index = np.unravel_index(int(score.argmin()), score.shape)
        print("Best fitting index: " + str(best_fit_index))

        # Plot a red rect around the found face
        plot_best_score_rect(IMAGE, TEMPLATE, best_fit_index[1], best_fit_index[0], filter_size)

        # Plot a 3d surface that describes the 'score' matrix
        plot_score_surface(score, best_fit_index, filter_size)
