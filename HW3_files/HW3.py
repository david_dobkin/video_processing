import cv2
import numpy as np
import numpy.matlib
from HW3_functions import *
import os
import glob
import re

"""
 HW 3, COURSE 0512-4263, TAU 2020

 PARTICLE FILTER TRACKING

 THE PURPOSE OF THIS ASSIGNMENT IS TO IMPLEMENT A PARTICLE FILTER TRACKER
 IN ORDER TO TRACK A RUNNING PERSON IN A SERIES OF IMAGES.

 IN ORDER TO DO THIS YOU WILL WRITE THE FOLLOWING FUNCTIONS:

 compNormHist(I, S)
 INPUT  = I (image) AND s (1x6 STATE VECTOR, CAN ALSO BE ONE COLUMN FROM S)
 OUTPUT = normHist (NORMALIZED HISTOGRAM 16x16x16 SPREAD OUT AS A 4096x1
                    VECTOR. NORMALIZED = SUM OF TOTAL ELEMENTS IN THE HISTOGRAM = 1)


 predictParticles(S_next_tag)
 INPUT  = S_next_tag (previously sampled particles)
 OUTPUT = S_next (predicted particles. weights and CDF not updated yet)


 compBatDist(p, q)
 INPUT  = p , q (2 NORMALIZED HISTOGRAM VECTORS SIZED 4096x1)
 OUTPUT = THE BHATTACHARYYA DISTANCE BETWEEN p AND q (1x1)

 IMPORTANT - YOU WILL USE THIS FUNCTION TO UPDATE THE INDIVIDUAL WEIGHTS
 OF EACH PARTICLE. AFTER YOU'RE DONE WITH THIS YOU WILL NEED TO COMPUTE
 THE 100 NORMALIZED WEIGHTS WHICH WILL RESIDE IN VECTOR W (1x100)
 AND THE CDF (CUMULATIVE DISTRIBUTION FUNCTION, C. SIZED 1x100)
 NORMALIZING 100 WEIGHTS MEANS THAT THE SUM OF 100 WEIGHTS = 1

 sampleParticles(S_prev, C)
 INPUT  = S_prev (PREVIOUS STATE VECTOR MATRIX), C (CDF)
 OUTPUT = S_next_tag (NEW X STATE VECTOR MATRIX)


 showParticles(I, S)
 INPUT = I (current frame), S (current state vector)
         W (current weight vector), i (number of current frame)
         ID 
"""



"""
General alogorithm flow:
Background:
At any given time for each particle the system described by:
s - system state
w - State's weights
c - State's CDF

S, W, C will be list of state, weights and CDFs for all particles



"""

numbers = re.compile(r'(\d+)')


def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


def all_code(xc_var=2.35, yc_var=1.82, half_width_var=0, half_height_var=0, vel_x_var=0.0025,
             vel_y_var=0.003, iteration=1, num_of_bins=16):

    id1 = "205825276"
    id2 = "314487455"

    our_ids = "HW3_{0}_{1}".format(id1, id2)
    image_dir_path = "{}{}Images".format(os.getcwd(), os.sep)
    # SET NUMBER OF PARTICLES
    N = 100
    # Initial Settings
    s_initial = [289,    # x center
                 135,    # y center
                 28,    # half width
                 55,    # half height
                 0,    # velocity x
                 0]    # velocity y

    # CREATE INITIAL PARTICLE MATRIX 'S' (SIZE 6xN)
    #[2, 2, <0.5, <0.5, 0.2, 0.2]
    # our_ids = str(iteration) + "-" + str(xc_var) + "_" + str(yc_var) + "_" + str(vel_x_var) + "_" + str(vel_y_var) + "_" + our_ids
    noise_var_list = [
                        xc_var,
                        yc_var,
                        half_width_var,
                        half_height_var,
                        vel_x_var,
                        vel_y_var
                     ]
    S = predictParticles(np.matlib.repmat(s_initial, N, 1).T, noise_var_list)
    # LOAD FIRST IMAGE
    I = cv2.imread(image_dir_path + os.sep + "001.png")
    # COMPUTE NORMALIZED HISTOGRAM
    q = calc_feature_vec(I, s_initial, feature_extract_method=compNormHist, method_args=num_of_bins, i=100)
    # COMPUTE NORMALIZED WEIGHTS (W) AND PREDICTOR CDFS (C)
    p = np.zeros((q.shape[0], N))
    w = np.zeros((N,))
    for i in range(N):
        p[:, i] = calc_feature_vec(I, S[:, i], feature_extract_method=compNormHist, method_args=16)
        w[i] = measure_feature_vec_dist(p[:, i], q, compare_method=compBatDist)
    w_sum = np.sum(w)
    w = w / w_sum
    C = np.cumsum(w)
    images_processed = 1
    # MAIN TRACKING LOOP
    image_name_list = os.listdir(image_dir_path)
    for image_name in sorted(glob.glob('Images' + os.sep + '*.png'), key=numericalSort)[1:]:
        S_prev = S
        # LOAD NEW IMAGE FRAME
        image_path = image_name
        I = cv2.imread(image_path)
        # SAMPLE THE CURRENT PARTICLE FILTERS
        S_next_tag = sampleParticles(S_prev, C)
        # PREDICT THE NEXT PARTICLE FILTERS (YOU MAY ADD NOISE
        S = predictParticles(S_next_tag, noise_var_list)
        # COMPUTE NORMALIZED WEIGHTS (W) AND PREDICTOR CDFS (C)
        p = np.zeros((q.shape[0], N))
        w = np.zeros((N,))
        for i in range(N):
            p[:, i] = calc_feature_vec(I, S[:, i], feature_extract_method=compNormHist, method_args=num_of_bins, i=i)
            w[i] = measure_feature_vec_dist(p[:, i], q, compare_method=compBatDist)
        w_sum = np.sum(w)
        w = w / w_sum
        C = np.cumsum(w)
        # CREATE DETECTOR PLOTS
        images_processed += 1
        if 0 == images_processed%10:
            showParticles(I, S, w, images_processed, our_ids)


if __name__ == '__main__':
    all_code()