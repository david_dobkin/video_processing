"""
 HW 3, COURSE 0512-4263, TAU 2020
 
 Function of HW3


 """
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import time
import os

XC_INDEX = 0
YC_INDEX = 1
HEIGHT_INDEX = 3
WIDTH_INDEX = 2


def predictParticles(prev_state, noise_var_list):
    # mean = 0
    # std = 1
    # num_samples = 1000
    # samples = numpy.random.normal(mean, std, size=num_samples)
    # todo: add normal noise instead
    cov = np.diag(noise_var_list)
    mean = np.zeros(shape=(prev_state.shape[0]))
    noise = np.random.multivariate_normal(mean, cov, prev_state.shape[1])
    #  Apply Aprior model
    A = np.array([[1, 0, 0, 0, 1, 0], [0, 1, 0, 0, 0, 1], [0, 0, 1, 0, 0, 0],
                  [0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1]])
    state = A.dot(prev_state) + noise.T
    return state


def compBatDist(p, q):
    # Pointwise multiplication:
    x = np.multiply(p, q)
    x = np.sqrt(x)
    x = 20 * np.sum(x)
    return math.exp(x)


"""
compNormHist(I, S)
 INPUT  = I (image) AND s (1x6 STATE VECTOR, CAN ALSO BE ONE COLUMN FROM S)
 OUTPUT = normHist (NORMALIZED HISTOGRAM 16x16x16 SPREAD OUT AS A 4096x1
                    VECTOR. NORMALIZED = SUM OF TOTAL ELEMENTS IN THE HISTOGRAM = 1)

The histogram is computed for the pixels in the rectangle sized width x height with the centers xc and yc.
You receive this information from 𝑠𝑖𝑛𝑖𝑡𝑖𝑎𝑙.
Looking at this sub-portion of I, you’ll notice the pixel intensities vary between 0 and 255 (8-bit).
We want to quantize this to 4 bits (0 to 15).
Next we look at each new combination of I_subportion(R,G,B) and we have a total of 163=4096 combinations of RGB values.
We’ll build a histogram vector such that each element in the vector describes the number of times this RGB combination appears.
Do this for 16x16x16 values and reshape it a 4096x1 vector.
Normalize this vector such that the sum of all elements is equal to 1.
"""


def crop_image(img, s):
    cropped_image = img[int(round(s[YC_INDEX] - s[HEIGHT_INDEX])): int(round(s[YC_INDEX] + s[HEIGHT_INDEX])),
                        int(round(s[XC_INDEX] - s[WIDTH_INDEX])): int(round(s[XC_INDEX] + s[WIDTH_INDEX])), ...]
    return cropped_image


def imageQuantization(img, num_of_bins):
    img_quantized = np.array(img / num_of_bins, dtype=np.uint8)
    img_quantized_rows = img_quantized.reshape((img_quantized.shape[0] * img_quantized.shape[1],
     img_quantized.shape[2]))
    return img_quantized_rows


def compNormHist(cropped_img, num_of_bins=16):
    img_quantized_rows = imageQuantization(cropped_img, num_of_bins)
    histogram, _ = np.histogramdd(img_quantized_rows, num_of_bins)
    histogram = histogram.astype(np.uint8)
    histogram = np.reshape(histogram, (histogram.shape[0] * histogram.shape[1] * histogram.shape[2],))
    sum_histogram = np.sum(histogram)
    return histogram / sum_histogram


def calc_feature_vec(img, s, feature_extract_method, method_args, i=0):
    cropped_image = crop_image(img, s)
    feature_vec = feature_extract_method(cropped_image, num_of_bins=16)
    return feature_vec


# Generic function to calculate distance between features vectors.
# Gets comparison method
def measure_feature_vec_dist(p, q, compare_method):
    return compare_method(p, q)


def sampleParticles(s_prev, c):
    # r ~ U[0, 1]
    r = np.random.rand(s_prev.shape[1])
    s = np.zeros_like(s_prev)
    # Create new particles popultated according to C - CDF
    for n in range(c.shape[0]):
        for j in range(c.shape[0]):
            if r[n] <= c[j]:
                s[:, n] = s_prev[:, j]
                break
    return s



def showParticles(I, S, W, frame_number, ID_STRING):
    # Caluculate weigthed sum best state 
    W_mat = np.matlib.repmat(W, S.shape[0], 1)
    s_weighted_sum = np.sum(np.multiply(S, W_mat), axis=1)
    # Calucalte max weight best state
    s_max = S[:, np.argmax(W)]
    fig,ax = plt.subplots(1)
    # # Create a Rectangle patches
    best_states = [s_weighted_sum, s_max]
    edge_colors = ['g', 'r']
    for s, edge_color in zip(best_states, edge_colors):
        bottom_rect = s[YC_INDEX]- s[HEIGHT_INDEX]
        left_rect = s[XC_INDEX] - s[WIDTH_INDEX]
        rect_width = s[WIDTH_INDEX]*2
        rect_height = s[HEIGHT_INDEX]*2
        rect = patches.Rectangle((left_rect, bottom_rect), width=rect_width,
                                 height=rect_height, linewidth=1, edgecolor=edge_color, facecolor='none')
        # Add the patch to the Ax
        ax.add_patch(rect)
    ax.imshow(I)
    title = ID_STRING + "-" + str(frame_number)
    plt.title(title)
    plt.savefig("out" + os.sep + title + ".png")
    #plt.savefig("out/" + title + ".png")
    plt.show(block=False)
    plt.close()
    # ax[0].imshow(img)
    # ax[0].add_patch(rect)
    # ax[1].imshow(cropped_image)
    # plt.show(block=True)
    
    
